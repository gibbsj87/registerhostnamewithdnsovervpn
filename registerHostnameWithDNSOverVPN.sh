#!/bin/sh
#Last modified 5/13/2020
#Register Mac hostname with DNS over VPN

# interface to use
# GlobalProtect VPN:

#Domain where host is joined to
joined_domain="domain.company.com"

#grab hostname of computer
myhost=$(hostname -s)

#grab IP address of GlobalProtect interface
#subnet of VPN adapter here
new_ip_address=$(/sbin/ifconfig | grep '10.100'  | grep 'inet ' | cut -d: -f1 | awk '{print $2}')

#create tempfile for nsupdate to read
nsupdate_commands=$(mktemp nsupdate_XXXXXX)

echo "update delete ${myhost}.${joined_domain} A" > ${nsupdate_commands}
echo "update add ${myhost}.${joined_domain} 3600 A ${new_ip_address}" >> ${nsupdate_commands}
echo "send" >> ${nsupdate_commands}

#update DNS record for host using Kerberos credentials (-g flag)
/usr/bin/nsupdate -g ${nsupdate_commands} && rm ${nsupdate_commands}
