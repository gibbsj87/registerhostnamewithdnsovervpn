You must add the execute bit to the script by running chmod +x nameofthefile.sh from the Terminal app.  If you have left the default name for the script, then it would look like:

chmod +x registerHostnameWithDNSOverVPN.sh

Now that the execute bit is set, you can run the script.  From the terminal, type:

./registerHostnameWithDNSOverVPN.sh

If the script executes as expected, you should see no output. After the script has run, if you execute nslookup against your computer's hostname, the response should be the IP address of the GlobalProtect VPN client.
